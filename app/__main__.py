from controller import partie
from controller import afficher
from dao.joueur_dao import JoueurDAO
from model.joueur import Joueur
from model import utils
import time

afficher.page_d_accueil()
choix = 1

while choix != 3:
    afficher.bienvenue()
    choix = utils.controler_choix(1, 3)
    match choix:
        case 1:
            partie.run()
            print("\n\nLa partie vient de se terminer !!")
        case 2:
            nom = str(input("\nVeuillez saisir son nom : "))
            while True:
                try:
                    solde = float(input("\nVeuillez saisir son solde (en €) : "))
                    assert solde > 0
                except ValueError:
                    print("Veuillez entrer un solde positif")
                except AssertionError:
                    print("")
                else:
                    JoueurDAO().insert_joueur(Joueur(0, nom, solde))
                    print("\nVotre joueur a été inséré avec succès.")
                    break
        case 3:
            time.sleep(1)
            print("\n\nMerci d'avoir tester notre application. Au revoir !!")
    time.sleep(1)
