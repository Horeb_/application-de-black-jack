from typing import List
from model.client import Client
from model.joueur import Joueur
from dao.joueur_dao import JoueurDAO


def controler_mise(joueur) -> float:
    '''
        Fonction permettant de cadrer la mise que l'utilisateur

        Attribut/Return
        ---------
        mise : int
            La mise que le joueur souhaite risquer dans le jeu
    '''
    while True:
        try:
            mise = float(input("Mise : "))
            assert mise > 0
            assert not joueur._solde_est_inferieur_ou_egale_a_(mise)
        except ValueError:
            print("Veuillez entrer un nombre, s'il vous plait")
        except AssertionError:
            print(f"Veuillez entrer un nombre supérieur à 0 et inférieur à votre solde qui est égal à {joueur._get_solde()}€")
        else:
            break
    return mise


def controler_choix(borne_inf: int, borne_sup: int) -> int:
    '''
    Fonction permettant de cadrer le choix de l'utilisateur

        Attribut
        ---------
        borne_inf : int
        borne_sup : int

        Return :
        --------
        choix : int
    '''
    while True:
        try:
            choix = int(input("\nChoix : "))
            assert choix >= borne_inf
            assert choix <= borne_sup
        except ValueError:
            print("Veuillez entrer un nombre entier")
        except AssertionError:
            print(f"Entrer un nombre entre {borne_inf} et {borne_sup}")
        else:
            break
    return choix


def choix_joueur():
    '''
        Fonction permettant à l'utilisateur de choisir un joueur dans la base

        Return :
        --------
        Joueur : Joueur
            Le joueur choisi par l'utilisateur.
    '''
    print("\nVoici les joueurs disponibles : \n")
    print("Id \t\t Nom \t\t\t Solde\n")
    players = JoueurDAO().list_joueurs()
    for player in players:
        print(player[0], " \t\t ", player[1], " \t\t ", player[2], "\n")

    while True:
        try:
            choosen_id = int(input("Entrez l'id du joueur dont vous voulez prendre le contrôle : "))
            assert choosen_id in [player[0] for player in players]
        except ValueError:
            print("Veuillez entrer un nombre entier.")
        except AssertionError:
            print("Veuillez entrer un id valide.")
        else:
            break
    choosen_player = JoueurDAO().select_joueur(choosen_id)
    return Joueur(choosen_player[0], choosen_player[1], choosen_player[2])


def ajout_carte():
    '''
    Fonction offrant le choix à l'utilisateur d'ajouter une carte supplémentaire

        Return :
        --------
        choix : int
    '''
    while True:
        print("Voulez-vous une carte supplémentaire ?")
        print("1. Oui")
        print("2. Non")
        try:
            choix = int(input("Choix : "))
            assert choix >= 1
            assert choix <= 2
        except ValueError:
            print("Veuillez entrer un nombre entier")
        except AssertionError:
            print("Entrer un nombre entre 1 et 2")
        else:
            break
    return choix


def distribuer_carte(nbreCartes: int):
    '''
    Fonction permettant de distribuer les cartes au joueur ou au croupier.

    Attribut
    ---------
    nbreCartes: int
        nombre de cartes à tirer et à distribuer

        Return :
        --------
        Retourne une liste d'objets de type carte.
    '''
    client = Client()
    deckId = client.shuffle(3)
    return client.draw(deckId, nbreCartes)


def compter_point(liste: List) -> int:
    point = 0
    for carte in liste:
        point += carte.get_point()
    return point


def nombre_AS(liste: List):
    nombre = 0
    for carte in liste:
        if carte._get_valeur() == "AS":
            nombre += 1
    return nombre


def remunerer(joueur: Joueur, statut: str, mise: float) -> float:
    match statut:
        case 'blackjack':
            gain = round(mise * 1.5)
        case 'abandonner':
            gain = - round(mise / 2)
        case 'perdre':
            gain = - mise
        case 'gagner':
            gain = mise
        case 'egaliter':
            gain = 0
    joueur._ajouter_gain(gain)
    if joueur._get_solde() < 5:
        joueur._set_solde(5)
    JoueurDAO().modify_solde_joueur(joueur._get_id(), joueur._get_solde())
    return gain


def joueur_jouant():
    print("Voulez-vous créer un nouveau joueur pour jouer ? ")
    print("1 : Oui\n2 : Non")
    rep = input("Votre réponse : ")

    while rep not in ["1", "2"]:
        rep = input("Votre réponse entre 1 et 2 : ")

    match rep:
        case "1":
            nom = input("Nom : ")
            solde = float(input("Solde : "))
            return Joueur(0, nom, solde)

        case "2":
            return choix_joueur()
