from typing import Optional, List
from model.carte import Carte
from model.singleton import Singleton
import requests

class Client(metaclass=Singleton):
    def shuffle(self, nb_deck):
        '''
        Fonction permettant de mélanger un jeu de cartes et 
        de le mettre à disposition.

        Paramètres 
        ----------
        nb_deck : int
            nombre de deck de cartes dans le jeu de cartes à mélanger (et par extension, détermine le nombre de cartes).
            1 deck = 52 cartes.

        Retourne
        -------
        id_deck : str
            id du jeu de cartes mélangé et mis à disposition.
        '''
        rep = requests.get(f"https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count={nb_deck}")
        id_deck = rep.json()['deck_id']
        return id_deck



    def draw(self, id_deck, nb_cards) -> Optional[list]:
        '''
        Fonction permettant de tirer un nombre donnée de cartes
        dans un certain jeu de cartes.

        Paramètres 
        ----------
        id_deck : str
            id du jeu de cartes à utiliser pour le tirage

        nb_deck : int
            nombre de cartes à tirer dans le jeu de cartes indiqué

        Retourne
        -------
        Cartes : list[Carte]
            Liste d'objet de type cartes représentant les cartes tirées
        '''
        rep = requests.get(f"https://deckofcardsapi.com/api/deck/{id_deck}/draw/?count={nb_cards}").json()
        cartes = [Carte(card['code'], card['suit'], card['value']) for card in rep['cards']]
        return cartes
    




