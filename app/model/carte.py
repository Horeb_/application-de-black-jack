class Carte:
    """
    Cette classe définit une carte

    Attributs:
    ----------
    __id      : l'identifiant de la carte
    __couleur : la couleur de la carte (carreau, coeur, trèfle, pique)
    __valeur   : la valeur de la carte 

    Methodes:
    ---------
    None
    """
    def __init__(self, id: str, couleur: str, valeur: str):
        self.__id = id
        if couleur == "HEARTS":
            self.__couleur = "COEUR"
        if couleur == "SPADES":
            self.__couleur = "PIQUES"
        if couleur == "CLUBS":
            self.__couleur = "TREFLES"
        if couleur == "DIAMONDS":
            self.__couleur = "CARREAUX"

        if valeur == "KING":
            self.__valeur = "ROI"
        elif valeur == "QUEEN":
            self.__valeur = "REINE"
        elif valeur == "JACK":
            self.__valeur = "VALET"
        elif valeur == "ACE":
            self.__valeur = "AS"
        else:
            self.__valeur = valeur

    def _get_id(self):
        return self.__id

    def _get_couleur(self):
        return self.__couleur

    def _get_valeur(self):
        return self.__valeur

    def get_point(self):
        if self.__valeur in list(map(str, list(range(2, 11)))):
            return int(self.__valeur)
        if self.__valeur in ["ROI", "REINE", "VALET"]:
            return 10
        if self.__valeur == "AS":
            return 11

    id = property(_get_id)
    couleur = property(_get_couleur)
    valeur = property(_get_valeur)

    def __str__(self):
        return f"{self.__valeur} DE {self.__couleur}"


if __name__ == "__main__":
    c1 = Carte("10", "CLUBS", "ACE")
    print(c1)
    print(c1.get_point())