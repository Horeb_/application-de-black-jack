class Joueur:
    def __init__(self, id: int, nom: str, solde: float) -> None:
        '''
        Classe joueur décrit par un identifiant joueur,un nom de
        joueur et un solde.
        Ces attributs sont privés pour éviter leur modification
        par n'importe quel utilisateur.
        '''
        self.__id = id
        self.__nom = nom
        self.__solde = solde

    def _get_id(self) -> int:
        return self.__id

    def _set_id(self, new_id):
        self.__id = new_id

    def _get_nom(self) -> str:
        return self.__nom

    def _set_nom(self, new_nom):
        self.__nom = new_nom

    def _get_solde(self) -> float:
        return self.__solde

    def _set_solde(self, new_solde):
        self.__solde = new_solde

    id = property(_get_id, _set_id)
    nom = property(_get_nom, _set_nom)
    solde = property(_get_solde, _set_solde)

    '''
        Cette méthode permettra de contrôler la mise du joueur.
        Elle prend en entrée la mise du joueur et retourne vrai
        si la mise du joueur est acceptable.
    '''
    # @classmethod
    def _solde_est_inferieur_ou_egale_a_(self, mise: float):
        return self.__solde < mise

    # @classmethod
    def _ajouter_gain(self, gain: float):
        '''
            Cette méthode permettra de mettre à jour le solde d'un joueur
            lorsqu'il à participer à un jeu.
            Le paramètre gain peut être positif si le joueur à gagner sa
            partie et négatif en cas de perte.
        '''
        self.__solde += gain

    def __str__(self) -> str:
        """
            Cette méthode permet d'afficher un joueur selon un format.
        """
        message = "Le joueur {} s'appelle {} et a un solde de {}€."
        return message.format(self.id, self.nom, self.solde)
