DROP TABLE IF EXISTS joueur;
CREATE TABLE joueur (
id SERIAL PRIMARY KEY,
nom VARCHAR(50) DEFAULT 'Player UNKNOWN',
solde FLOAT
);

-- Insertion des joueurs
INSERT INTO joueur(nom, solde)
VALUES ('Player 1', 10.6);

INSERT INTO joueur(nom, solde)
VALUES ('Player 2', 15);

INSERT INTO joueur(nom, solde)
VALUES ('Player 3', 25);

INSERT INTO joueur(nom, solde)
VALUES ('Player 4', 12.8);

INSERT INTO joueur(nom, solde)
VALUES ('Player 5', 20);

INSERT INTO joueur(nom, solde)
VALUES ('Player 6', 18);