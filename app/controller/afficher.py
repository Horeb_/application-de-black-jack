from typing import List

def page_d_accueil():
    print("\n\n#################################################################################################")
    print("#                                BIENVENUE AU JEU DE                                            #")
    print("#                                                                                               #")
    print("#  ==        =           =          ===    =   =             =        =            ===  =   =   #")
    print("#  ==        =          =  =       =       =  =              =       =  =         =     =  =    #")
    print("#  ==        =         =    =     =        = =    =====      =      =    =       =      = =     #")
    print("#  == = = =  =        = = = ==    =        =  =              =     = = = ==     =       =  =    #")
    print("#  ==     =  =       =        =    =       =   =        =    =    =        =     =      =   =   #")
    print("#  == = = =  ====   =          =    ===    =    =       =====    =          =     ====  =    =  #")
    print("#################################################################################################\n\n")


def affiche_debut():
    print("\n\nPour participer au jeu il va falloir miser")
    print("Combien voulez-vous misez ?")


def affiche_suite():
    print("\n\nLes joueurs ayant déja misés nous allons passer à la distribution des différentes cartes.")
    print("Distribution des cartes, veuillez patienter...\n\n")



def affiche_choix():
    print("\n\nQue voulez-vous faire ?")
    print("1. Demander une carte supplémentaire")
    print("2. Rester avec vos différentes cartes")
    print("3. Doubler sa mise en recevant une carte supplémentaire")
    print("4. Abandonner la partie")

def bienvenue():
    print("\n\nQue voulez-vous faire ?")
    print("1. Jouer")
    print("2. Insérer nouveau un joueur")
    print("3. Quitter")

def afficher_cartes(liste: List):
    #print("\nVos différentes cartes sont : ")
    for element in liste:
        print("\n\t", element)
