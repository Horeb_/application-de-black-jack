from controller import afficher
from model import utils
import time


def demander_une_carte(cartes, nbre_AS: int, point: int):
    print("\nDistribution d'une carte, veuillez patienter...\n")
    cartes += utils.distribuer_carte(1)
    if cartes[-1]._get_valeur() == "AS":
        nbre_AS += 1
    print("\nVos différentes cartes sont : ")
    afficher.afficher_cartes(cartes)
    point += cartes[-1].get_point()
    message = ""
    statut = ""
    if (point > 21) and (nbre_AS == 0):
        statut = "perdre"
        message = f"\nPas de chance!!! Vous avez {point} points donc vous perdez :(\n"
    else:
        if (point > 21) and (nbre_AS != 0):
            point -= 10
            nbre_AS -= 1
        print(f"\nVous avez {point} points.\n\n")
        choix2 = utils.ajout_carte()
        while choix2 != 2:
            print("\nDistribution d'une carte, veuillez patienter...\n")
            cartes += utils.distribuer_carte(1)
            if cartes[-1]._get_valeur() == "AS":
                nbre_AS += 1
            print("\nVos différentes cartes sont : ")
            afficher.afficher_cartes(cartes)
            point += cartes[-1].get_point()
            if (point > 21) and (nbre_AS == 0):
                statut = "perdre"
                message = f"\nPas de bol, vous avez {point} points donc vous perdez.\n"
                break
            else:
                if (point > 21) and (nbre_AS != 0):
                    point -= 10
                    nbre_AS -= 1
                    if (point > 21) and (nbre_AS == 0):
                        statut = "perdre"
                        message = f"\nPas de bol, vous avez {point} points donc vous perdez.\n"
                        break

            print(f"\nVous avez {point} points.\n\n")
            choix2 = utils.ajout_carte()
    return (cartes, point, statut, message)


def run():

    joueur = utils.choix_joueur()

    afficher.affiche_debut()

    mise_joueur = utils.controler_mise(joueur)

    afficher.affiche_suite()

    cartes_joueur = utils.distribuer_carte(2)
    AS_joueur = utils.nombre_AS(cartes_joueur)
    cartes_croupier = utils.distribuer_carte(1)
    print("\nVos différentes cartes sont : ")
    afficher.afficher_cartes(cartes_joueur)

    point_joueur = utils.compter_point(cartes_joueur)

    statut = ""

    if point_joueur == 21:
        statut = "blackjack"
        print("\nC'est au tour du Croupier de tirer des cartes.")
        print("Tirage d'une carte par le croupier, veuillez patienter...\n")
        cartes_croupier += utils.distribuer_carte(1)
        print("\nLes cartes du croupier sont :\n")
        afficher.afficher_cartes(cartes_croupier)
        point_croupier = utils.compter_point(cartes_croupier)
        if point_croupier == 21:
            message = f"\nVous avez {point_joueur} points et le croupier a {point_croupier} donc vous êtes à égalité.\nVous récupérez votre mise.\n"
        else:
            message = "\nFélicitations, vous avez fait un blackjack. Vous gagnez donc 1.5 fois votre mise.\n"
    else:
        if point_joueur > 21:  # Cas où le joueur à 2 AS (un vaut 1 et l'autre vaut 11)
            AS_joueur = utils.nombre_AS(cartes_joueur)
            point_joueur -= 10
            AS_joueur -= 1
        print(f"\nVous avez {point_joueur} points.")
        time.sleep(1)
        afficher.affiche_choix()
        choix = utils.controler_choix(1, 4)
        match choix:
            case 1:
                cartes_joueur, point_joueur, statut,\
                    message = demander_une_carte(cartes_joueur, AS_joueur, point_joueur)
            case 2:
                point_joueur = utils.compter_point(cartes_joueur)
            case 3:
                if joueur._solde_est_inferieur_ou_egale_a_(mise_joueur*2):
                    print("\n\nVotre solde est insuffisant pour doubler votre mise\n")
                    time.sleep(1)
                    print("Vous pouvez soit : ")
                    print("1. Demander une carte supplémentaire")
                    print("2. Rester avec vos différentes cartes")
                    print("3. Abandonner la partie")
                    print("\nQuel est votre choix ?")
                    choix2 = utils.controler_choix(1, 3)
                    match choix2:
                        case 1:
                            cartes_joueur, point_joueur, statut,\
                                 message = demander_une_carte(cartes_joueur, AS_joueur, point_joueur)
                        case 2:
                            point_joueur = utils.compter_point(cartes_joueur)
                        case 3:
                            statut = "abandonner"
                            message = f"\nPoule Mouillé !! Vous perdez la moitié de votre mise qui est de {mise_joueur/2}€\n"
                else:
                    mise_joueur *= 2
                    print("\nDistribution d'une carte, veuillez patienter...\n")
                    cartes_joueur += utils.distribuer_carte(1)
                    AS_joueur = utils.nombre_AS(cartes_joueur)
                    print("\nVos différentes cartes sont : ")
                    afficher.afficher_cartes(cartes_joueur)
                    point_joueur = utils.compter_point(cartes_joueur)
                    if point_joueur > 21:
                        if AS_joueur == 0:
                            statut = "perdre"
                            message = f"\nOh la poisse!!! Vous avez {point_joueur} points donc vous perdez :(\n"
                        else:
                            point_joueur -= 10

            case 4:
                statut = "abandonner"
                message = f"\nPoule Mouillé !! Vous perdez la moitié de votre mise qui est de {mise_joueur/2}€\n"

    if not statut:
        time.sleep(2)
        print("\nC'est au tour du Croupier de tirer des cartes.")
        print("Tirage d'une carte par le croupier, veuillez patienter...\n")
        cartes_croupier += utils.distribuer_carte(1)
        print("\nLes cartes du croupier sont :\n")
        afficher.afficher_cartes(cartes_croupier)
        point_croupier = utils.compter_point(cartes_croupier)
        while point_croupier < 17:
            print(f"\nLe Croupier a {point_croupier} points inférieur à 17 donc il tire encore une autre carte.")
            print("Tirage d'une carte par le croupier, veuillez patienter...\n")
            cartes_croupier += utils.distribuer_carte(1)
            time.sleep(2)
            print("\nLes cartes du croupier sont :\n")
            afficher.afficher_cartes(cartes_croupier)
            point_croupier = utils.compter_point(cartes_croupier)
        if point_croupier > 21:
            statut = "gagner"
            message = f"\nLe croupier a {point_croupier} points qui est supérieur à 21 donc vous gagnez.\n"
        elif point_croupier < point_joueur:
            print("\nVos différentes cartes sont : ")
            afficher.afficher_cartes(cartes_joueur)
            statut = "gagner"
            message = f"\nBravo! Vous avez {point_joueur} points et le croupier a {point_croupier} donc vous gagnez.\n"
        elif point_croupier > point_joueur:
            print("\nVos différentes cartes sont : ")
            afficher.afficher_cartes(cartes_joueur)
            statut = "perdre"
            message = f"\nDésolé, vous avez {point_joueur} points et le croupier a {point_croupier} donc vous perdez :(.\n"
        else:
            print("\nVos différentes cartes sont : ")
            afficher.afficher_cartes(cartes_joueur)
            statut = "egaliter"
            message = f"\nVous avez {point_joueur} points et le croupier a {point_croupier} donc vous êtes à égalité.\nVous récupérez votre mise.\n"

    gain = utils.remunerer(joueur, statut, mise_joueur)
    time.sleep(2)
    print(message)
