from dao.dbconnection import DBConnection


class JoueurDAO:
    '''
    Classe permettant de récupérer les données demandées.
    '''

    def insert_joueur(self, joueur):
        '''
        Fonction permettant d'insérer des données dans la table
            Joueur du serveur distant.

        Attributs
        ---------
        joueurs : list
            ensemble des joueurs à insérer de format
            [Joueur(id, nom, solde), Joueur(id, nom, solde), ...]
        '''

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO joueur(nom, solde) VALUES \
                        (%(nom)s, %(solde)s)",
                    {"nom": joueur._get_nom(), "solde": joueur._get_solde()}
                )

    def select_joueur(self, id_joueur: id):
        '''
        Fonction permettant de récupérer un joueur dans la base de données
        pour lui permettre de participer au jeu.

        Attribut
        ---------
        id_joueur : id
            id du joueur dont on veut récupérer les données
        '''
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM  joueur" "\nWHERE id = %(id_joueur)s",
                    {"id_joueur": id_joueur}
                )
                resultat = cursor.fetchone()
        if resultat:
            joueur = [resultat["id"], resultat["nom"], resultat["solde"]]
        return joueur

    def list_joueurs(self):
        '''
        Fonction permettant de récupérer tous les joueurs de la database

        Return
        ---------
        liste : list
            liste des joueurs représentés par des listes de type (id, nom, solde)
        '''
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("SELECT * FROM joueur\nORDER BY id;")
                resultat = cursor.fetchall()
        liste = list()
        if resultat:
            for row in resultat:
                liste.append([row["id"], row["nom"], row["solde"]])
        return liste

    def modify_solde_joueur(self, id_joueur: int, new_solde: int):
        '''
        Fonction permettant de modifier le solde d'un joueur existant
        dans la database

        Attribut
        --------
        new_solde : int
            nouvelle solde à remplacer
        id_joueur : str
            id du joueur dont on veut récupérer les données
        '''
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("UPDATE joueur\
                    \nSET solde = %(new_solde)s \
                    \nWHERE id = %(id_joueur)s",
                               {"id_joueur": id_joueur,
                                "new_solde": new_solde}
                               )
