import unittest

from app.model.joueur import Joueur


class TestJoueur(unittest.TestCase):
    
    def test_solde_est_inferieur_ou_egale_a_(self):
        joueur1 = Joueur("1", "salut", 100)
        self.assertEqual(joueur1._solde_est_inferieur_ou_egale_a_(500), True)
    
    def test_ajouter_gain(self):
        joueur1 = Joueur("1", "salut", 100)
        joueur1._ajouter_gain(20)
        self.assertEqual(joueur1._get_solde(), 120)


if __name__ == '__main__':
    unittest.main()