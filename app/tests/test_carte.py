import unittest

from app.model.carte import Carte

class TestCarte(unittest.TestCase):
    def test_get_point(self):
        carte = Carte("C1", "SPADES", "KING")
        self.assertEqual(carte.get_point(), 10)

    def test_get_valeur(self):
        carte = Carte("C1", "SPADES", "KING")
        self.assertEqual(carte._get_valeur(), "ROI")

if __name__ == '__main__':
    unittest.main()