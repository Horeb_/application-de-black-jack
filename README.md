# Application de Black Jack

## Objectif, organisation et fonctionnement

### Objectif

BlackJack est un jeu de carte qui met en opposition un croupier et plusieurs joueurs. 1 ou plusieurs (8 au maximum) lots de 52 cartes sont mis en jeu et un point est attribué à chaque carte :
toute carte de 2 à 10 garde leur valeur, les cartes valet, reine et roi ont la valeur 10 et enfin l'As peut avoir une valeur de 1 ou 11 selon l'intérêt du joueur.

Les joueurs ont d'abord deux cartes chacun et ont la possibilité s'ils le souhaitent d'ajouter d'autres cartes. Le croupier tire ses cartes jusqu'à ce qu'il dépasse 16 et s'arrête. L'objectif de chaque joueur est de se rapprocher le plus possible de 21 sans le dépasser, et de dépasser également la valeur des cartes du croupier.

Pour plus d'informations sur les règles du jeu voir https://fr.wikipedia.org/wiki/Blackjack_(jeu).

### Organisation du code

![Structure](img/structure.png)

_Figure 2 : Structure_

- [ ] controller : contient des fichiers qui gèrent l'affichage et l'interaction avec l'utilisateur

- [ ] dao : communique avec la base de données des joueurs afin sauvegarder, et mettre à jour les informations des joueurs 

- [ ] model : implémente les différentes classes du diagramme de classe ainsi que les fichiers contenant le code du gameplay du jeu

- [ ] tests : fait deux tests unitaires.

### Fonctionnement

Ci-dessous le diagramme d'activité qui décrit le déroulement d'une partie du jeu de black-jack:

![Diagramme d'activite](img/diagramme_activite.png)

_Figure 2 : Diagramme d'activité_


## Démarrage rapide

Pour jouer au jeu de BlackJack, il faut (_Toutes les commandes doivent être lancées par un terminal_):

- cloner le dépôt git : 
```
git clone https://gitlab.com/Horeb_/application-de-black-jack.git
```

- installer les dépendances, en s'étant au préalable placé dans le dossier de l'application, via les commandes:
```
cd application-de-black-jack
pip install -r requirements.txt
```

- ouvrir un serveur sur PostgreSQL , exécuter le code du fichier `init_db.sql`

- définir l'environnement de travail dans un fichier `.env` dans le dossier `app`.
Pour cela, il faut adapter dans le fichier `.env` les variables ci-dessous à votre environnement de travail
```
PASSWORD = 
HOST = 
PORT = 
DATABASE = 
USER = 
```

- se placer dans le dossier `app`:
```
cd app
```

- enfin lancer le jeu :
```
python .
```

## Le jeu de BlackJack

BlackJack est un jeu de carte qui met en opposition un croupier et plusieurs joueurs. 1 ou plusieurs (8 au maximum) lots de 52 cartes sont mis en jeu et un point est attribué à chaque carte :
toute carte de 2 à 10 garde leur valeur, les cartes valet, reine et roi ont la valeur 10 et enfin l'As peut avoir une valeur de 1 ou 11 selon l'intérêt du joueur.

Les joueurs ont d'abord deux cartes chacun et ont la possibilité s'ils le souhaitent d'ajouter d'autres cartes. Le croupier tire ses cartes jusqu'à ce qu'il dépasse 16 et s'arrête. L'objectif de chaque joueur est de se rapprocher le plus possible de 21 sans le dépasser, et de dépasser également la valeur des cartes du croupier.

Pour plus d'informations sur les règles du jeu voir https://fr.wikipedia.org/wiki/Blackjack_(jeu).





## Auteurs et remerciements
Nos remerciements à M. Antoine BRUNETTI pour son aide et son soutien.
Les auteurs (élèves 2A ENSAI)
- AGONNOUDE Tom
- AMOUSSOU Kokou
- SAWADOGO Julien
- SEIDOU Horeb

## License
Appartient aux auteurs et à l'ENSAI.
